#include <stdio.h>

struct student
{
    char name[50];
    char subject[50];
    float marks;
};

int main()
{
    int i, n;
    printf("Enter how many record you want to store: ");
    scanf("%d", &n);
    struct student s[n];
    printf("Enter name, subject, marks below: \n\n");

    // storing data
    for(i = 0; i < n; ++i)
    {
        printf("Enter %d record: \n", i + 1);

        printf("First name: ");
        scanf("%s", &s[i].name);
        printf("Subject: ");
        scanf("%s", &s[i].subject);
        printf("Marks: ");
        scanf("%f", &s[i].marks);
        printf("\n");
    }
    // displaying data
    printf("\t Name \t \t Marks \t Subject \n");
    for(i = 0; i <= 5; ++i)
        printf("\t %s \t \t %.2f \t %s \n", s[i].name, s[i].marks, s[i].subject);
        return 0;
}
